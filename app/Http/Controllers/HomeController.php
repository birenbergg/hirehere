<?php

namespace App\Http\Controllers;

use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user() != null)
        {
            if (Auth::user()->is_admin)
            {
                // $posts = Post::with('user')->where('user_id', Auth::user()->id)->orWhere('is_hidden', 0)->get();
                $posts = Post::with('user')->get();
            }
            else
            {
                $posts = Post::with('user')->where('user_id', Auth::user()->id)->orWhere('is_approved', 1)->where('is_hidden', 0)->get();
            }

            for($i = 0; $i < count($posts); $i++)
            {
                if($posts[$i]->user_id == Auth::user()->id)
                {
                    $posts[$i]->self = true;
                }
            }
        }
        else
        {
            $posts = Post::with('user')->where('is_approved', 1)->where('is_hidden', 0)->get();
        }

        return view('home', ['posts' => $posts]);
    }
    
    public function updatePost($id = 'undefined')
    {
        if ($id == 'undefined') {
            $post = new Post();
        } else {
            $post = Post::find($id);
        }

        $post->title = Input::get('title');
        $post->description = Input::get('description');
        $post->price = Input::get('price');
        $post->user_id = Auth::user()->id;
        $post->is_approved = Auth::user()->is_admin;
        
        if ($id == 'undefined') {
            $post->save();
        } else {
            $post->update();
        }

        return $post;
    }
    
    public function toggleVisibility($id)
    {
        $post = Post::find($id);

        $post->is_hidden = !$post->is_hidden;
        
        $post->update();
    }
    
    public function toggleApproval($id)
    {
        $post = Post::find($id);

        $post->is_approved = !$post->is_approved;
        
        $post->update();
    }

    public function deletePost($id)
    {
        $post = Post::find($id);
        $post->delete();
    }
}
