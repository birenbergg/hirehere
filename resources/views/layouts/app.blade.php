<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HireHere</title>

    <meta name="viewport" content="width=device-width, initial-scale=1"><link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon"/>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/my-modal.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" ng-app="hh" ng-controller="hhController" >
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="logo.png" alt="" style="height:100%">
                    </a>
                </div>

                @guest
                @else
                <ul class="nav navbar-nav">
                    <li ng-class="{ active: filterTerm == '' }" ng-click="filterTerm = ''"><a href="#">All Posts</a></li>
                    <li ng-class="{ active: filterTerm == 'self' }" ng-click="filterTerm = 'self'"><a href="#">Your Posts</a></li>
                    <li ng-class="{ active: filterTerm == 'approve' }" ng-show="{!! Auth::user()->is_admin !!}" ng-click="filterTerm = 'approve'"><a href="#">Post to Approve</a></li>
                    <li><a href="#" ng-click="showModal({})">New Post</a></li>
                </ul>
                @endguest


                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li><a class="no-hover">Hello, {{ Auth::user()->name }}</a></li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script   src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/ng-app.js') }}"></script>
</body>
</html>
