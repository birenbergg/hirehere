@extends('layouts.app')

@section('content')
<div class="container" ng-init="posts = {{ $posts }}">

    <div class="row" style="margin-top:20px;">
        <div class="panel" ng-repeat="p in posts | myFilter: filterTerm | orderBy:'-created_at'" ng-class="{ 'panel-primary': !p.is_hidden && p.is_approved, 'panel-info': p.is_hidden, 'panel-warning': !p.is_approved }">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span style="font-weight: bold;">@{{ p.title }}</span>
                    <div style="font-style: italic; margin-top: 1em;">
                        <span ng-if="p.is_hidden">Hidden</span>
                    </div>
                    <div style="font-style: italic; margin-top: 1em;">
                    <span ng-if="!p.is_approved">Waiting for admin approval</span>
                    </div>
                </h3>
            </div>
            <div class="panel-body" style="white-space: pre-line;">@{{ p.description }}</div>
            <div class="panel-body buttons-area">
                @guest
                    Created by @{{ p.user.name }}.
                @else
                    Created by @{{ p.self ? 'you' : p.user.name }}.
                    <div class="buttons-container">
                        <button ng-if="p.self" type="button" class="btn btn-default" ng-click="toggleVisibility(p.id)">@{{ p.is_hidden ? 'Unhide' : 'Hide' }}</button>
                        <button ng-if="{!! Auth::user()->is_admin !!} && !p.self" type="button" class="btn btn-default" ng-class="{ 'btn-success': !p.is_approved, 'btn-warning': p.is_approved }" ng-click="toggleApproval(p.id)">@{{ p.is_approved ? 'Disapprove' : 'Approve' }}</button>
                        <button ng-if="p.self" type="button" class="btn btn-primary" ng-click="showModal(p)">Edit</button>
                        <button ng-if="p.self" type="button" class="btn btn-danger" ng-click="deletePost(p)">Delete</button>
                    </div>
                @endguest
            </div>
            <div class="panel-footer" ng-if="p.price">
                <strong>
                    Price: $@{{ p.price }}
                </strong>
            </div>
        </div>

        <!-- Modal -->
        <div id="my-modal" class="my-modal-wrapper">
            <div class="my-modal">

                <!-- Modal content-->
                <div class="my-modal-content">
                    <div class="my-modal-header">
                        <h2 class="my-modal-title">@{{ selectedPost.id == undefined ? 'Add' : 'Edit' }} Post</h2>
                    </div>
                    <div class="my-modal-body">
                            <div class="my-input-group">
                                <div class="label">Title</div>
                                <div class="input-wrapper"><input id="title" ng-model="selectedPost.title" value="@{{ selectedPost.title }}" /></div>
                            </div>
                            
                            <div class="my-input-group">
                                <div class="label">Decription</div>
                                <div class="input-wrapper"><textarea id="description" ng-model="selectedPost.description" value="@{{ selectedPost.description }}"></textarea></div>
                            </div>

                            <div class="my-input-group">
                                <div class="label">Price</div>
                                <div class="input-wrapper"><input id="price" ng-model="selectedPost.price" value="@{{ selectedPost.price }}" /></div>
                            </div>
                    </div>
                    <div id="my-modal-buttons-wrapper">
                        <button type="button" ng-click="updatePost()" class="btn btn-success" style="margin-right: 10px;">Save</button>
                        <button type="button" ng-click="hideModal()" class="btn btn-default">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
