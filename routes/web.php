<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
	Route::post('/posts/update/{id}', 'HomeController@updatePost')->name('post.update');
	Route::delete('/posts/delete/{id}', 'HomeController@deletePost')->name('post.delete');
	Route::post('/posts/toggle-visibility/{id}', 'HomeController@toggleVisibility')->name('post.toggleVisibility');
	Route::post('/posts/toggle-approval/{id}', 'HomeController@toggleApproval')->name('post.toggleApproval');
});